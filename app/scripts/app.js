;(function(angular, undefined){

'use strict';

angular
    .module('todoApp', [
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.select',
        'ngSanitize'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('tasks', {
                abstract: true,
                url: '/tasks',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .state('tasks.list', {
                url: '/list',
                views: {
                    'list@tasks': {
                        templateUrl: 'views/list.html',
                        controller: 'ListCtrl'        
                    }
                }
            })
            .state('tasks.edit', {
                url: '/edit/:id',
                data: {
                    action: 'edit'
                },
                views: {
                    'list@tasks': {
                        templateUrl: 'views/list.html',
                        controller: 'ListCtrl'        
                    },
                    'edit@tasks': {
                        templateUrl: 'views/edit.html',
                        controller: 'EditCtrl'
                    }
                }
            })
            .state('tasks.add', {
                url: '/add',
                data: {
                    action: 'add'
                },
                views: {
                    'list@tasks': {
                        templateUrl: 'views/list.html',
                        controller: 'ListCtrl'        
                    },
                    'edit@tasks': {
                        templateUrl: 'views/edit.html',
                        controller: 'EditCtrl'
                    }
                }
            });

        $urlRouterProvider
            .when('/', '/tasks')
            .when('/tasks', '/tasks/list')
            .otherwise('/tasks/list');
    });
})(window.angular);
