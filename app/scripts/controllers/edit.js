;(function(angular, _, undefined){
    'use strict';

    angular.module('todoApp')
        .controller('EditCtrl', function ($scope, $resource, $state) {

            function setupDatepicker() {
                $scope.openCalendar = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };

                $scope.dateOptions = {
                    startingDay: 1
                };
            }

            function setupUsers() {
                $resource('/api/users').get().$promise.then(function(response){
                    $scope.people = response.users;
                });

                $scope.formAssignee = {};
            }

            function setupAction() {
                $scope.action = $state.current.data.action;
                $scope.taskId = ($state.current.data.action === 'edit') ? $state.params.id : '';
            }

            function populateEditForm() {
                if ($scope.action === 'edit') {
                    var task = _.find($scope.tasks, 'id', $scope.taskId);
                    if (!task) {
                        $state.go('tasks.list');
                        return;
                    }

                    $scope.formDescription = task.description;
                    $scope.formDueDate = moment(task.dueDate).format('YYYY-MM-DD');
                    $scope.formAssignee.name = task.assignee;
                }
            }

            $scope.addOrEditTask = function(action, taskId) {
                if (action === 'edit') {
                    var task = _.find($scope.tasks, 'id', taskId);

                    task.description = $scope.formDescription;
                    task.dueDate = $scope.formDueDate;
                    task.assignee = $scope.formAssignee.name;
                } else {
                    $scope.tasks.push({
                        id:          _.uniqueId(),
                        description: $scope.formDescription,
                        dueDate:     $scope.formDueDate,
                        assignee:    $scope.formAssignee.name,
                        completed:   false
                    });
                }

                $state.go('tasks.list');

                return _.last($scope.tasks).id;
            };

            setupDatepicker();
            setupUsers();
            setupAction();
            populateEditForm();
        })
        .filter('capitalize', function() {
            return function(input) {
                return _.capitalize(input);
            };
        });
})(window.angular, window._);
