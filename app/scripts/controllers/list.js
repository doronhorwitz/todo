;(function(angular, _, undefined){
    'use strict';

    angular.module('todoApp')
        .controller('ListCtrl', function () {
        })
        .directive('taskList', function(){
            return {
                restrict: 'E',
                templateUrl: 'views/task-list.html',
                transclude: true,
                controller: function($scope, $state) {
                    $scope.numTasks = function(forCompleted) {
                        return _.filter($scope.tasks, 'completed', forCompleted).length;
                    };
                    $scope.editTask = function(id) {
                        $state.go('tasks.edit', {
                            id: id
                        });
                    };
                },
                scope: {
                    forCompleted: '=',
                    tasks: '='
                }
            };
        });
})(window.angular, window._);
