;(function(angular, undefined){
    'use strict';

    angular.module('todoApp')
        .controller('MainCtrl', function ($scope) {
            $scope.tasks = [];
        });
})(window.angular);
