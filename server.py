from flask import Flask
from flask.ext import restful

app = Flask(__name__)
api = restful.Api(app)

class TodoServer(restful.Resource):
    def get(self):
        return {'users': [
        	'Alice Cooper',
        	'Bob Geldof',
        	'Chris Cornell',
        	'Deborah Dyer'

        ]}

api.add_resource(TodoServer, '/api/users')

if __name__ == '__main__':
    app.run(debug=True)