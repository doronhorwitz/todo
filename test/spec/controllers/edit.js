'use strict';

describe('Controller: EditCtrl', function () {

    // load the controller's module
    beforeEach(module('todoApp'));

    var EditCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, $state) {
        scope = $rootScope.$new();
        scope.tasks = [];
        $state.current = {
            data: {
                action: 'psuh'
            }
        };
        EditCtrl = $controller('EditCtrl', {
            $scope: scope,
            $state: $state
        });
    }));

    it('Should add a task to the list', function () {
        var date = new Date();
        scope.formDescription = 'A cool task';
        scope.formDueDate = date;
        scope.formAssignee = {
            name: 'Deborah Dyer'
        };

        var id = scope.addOrEditTask('add');
        expect(scope.tasks.length).toBe(1);
        expect(scope.tasks[0]).toEqualData({
            id: id,
            description: 'A cool task',
            dueDate: date,
            assignee: 'Deborah Dyer',
            completed: false
        })
    });

    it('Should edit a task in the list', function () {
        var date = new Date();
        scope.formDescription = 'A cool task';
        scope.formDueDate = date;
        scope.formAssignee = {
            name: 'Deborah Dyer'
        };
        var id = scope.addOrEditTask('add');

        var newerDate = new Date();
        scope.formDescription = 'Edited task';
        scope.formDueDate = newerDate;
        scope.formAssignee = {
            name: 'Alice Cooper'
        };
        scope.addOrEditTask('edit', id);

        expect(_.find(scope.tasks, 'id', id)).toEqualData({
            id: id,
            description: 'Edited task',
            dueDate: newerDate,
            assignee: 'Alice Cooper',
            completed: false
        })
    });
});
