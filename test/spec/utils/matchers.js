 beforeEach(function() {
    jasmine.addMatchers({
        toEqualData: function toEqualData(util, customEqualityTesters) {
            return {
                compare: function compare(actual, expected) {
                    var result = {
                        pass: angular.equals(actual, expected),
                    };
                    result.message = result.pass ? 'object parameters match' : 'Expected ' + angular.toJson(actual) + ' to equal ' + angular.toJson(expected);
                    return result;
                }
            };
        }
    });
});